﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NymityAssessment.Domain.Entities;
using NymityAssessment.Domain.Interfaces;
using NymityAssessment.Web.Controllers;
using NymityAssessment.Web.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Web.Http;

namespace NymityAssessment.Web.Tests.Controllers
{
    [TestClass]
    public class ProductsControllerTest
    {
        public readonly IUnitOfWork mockUnitOfWork;

        public ProductsControllerTest()
        {
            AutoMapperConfig.Initialize();

            //Configure Mocks
            var category1 = new Category("Category 1", "");
            category1.SetCustomId(1);
            var category2 = new Category("Category 2", "");
            category2.SetCustomId(2);
            var categories = new List<Category>() { category1, category2 };

            var product1 = new Product("Product 1", null, category1);
            product1.SetCustomId(1);
            var product2 = new Product("Product 2", null, category2);
            product2.SetCustomId(2);
            var products = new List<Product>() { product1, product2 };

            Mock<IUnitOfWork> unitMoc = new Mock<IUnitOfWork>();

            //Setup some product repository methods
            unitMoc.Setup(m => m.Products.GetById(It.IsAny<int>())).Returns((int i) => products.FirstOrDefault(p => p.ProductID == i));
            unitMoc.Setup(m => m.Categories.GetById(It.IsAny<int>())).Returns((int i) => categories.FirstOrDefault(c => c.CategoryID == i));
            unitMoc.Setup(m => m.Products.Find(It.IsAny<Expression<System.Func<Product, bool>>>())).Returns((Expression<System.Func<Product, bool>> criteria) => products.Where(criteria.Compile()));

            this.mockUnitOfWork = unitMoc.Object;
        }

        [TestMethod]
        public void GetCategoryById()
        {
            var categoryId = 1;
            var category = this.mockUnitOfWork.Categories.GetById(categoryId);

            Assert.IsNotNull(category);
            Assert.IsInstanceOfType(category, typeof(Category));
            Assert.AreEqual(categoryId, category.CategoryID);
        }

        [TestMethod]
        public void GetProductById()
        {
            var productId = 2;
            var product = this.mockUnitOfWork.Products.GetById(productId);

            Assert.IsNotNull(product);
            Assert.IsInstanceOfType(product, typeof(Product));
            Assert.AreEqual(productId, product.ProductID);
        }

        [TestMethod]
        public void GetProductByName()
        {
            var products = this.mockUnitOfWork.Products.Find(p => p.ProductName == "Product 1");

            Assert.IsNotNull(products);
            Assert.AreEqual(1, products.Count());
        }

        [TestMethod]
        public void HttpGetProduct()
        {
            // Arrange
            var controller = new ProductsController(this.mockUnitOfWork);

            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            // Act
            var response = controller.Get(1) as System.Web.Http.Results.OkNegotiatedContentResult<ProductViewModel>;

            // Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Content);
            Assert.IsInstanceOfType(response.Content, typeof(ProductViewModel));
        }

        [TestMethod]
        public void HttpGetProductByCategory()
        {
            // Arrange
            var controller = new ProductsController(this.mockUnitOfWork);

            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            // Act
            var response = controller.GetProductsByCategory(2) as System.Web.Http.Results.OkNegotiatedContentResult<List<ProductViewModel>>;

            // Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Content);
            Assert.IsInstanceOfType(response.Content, typeof(List<ProductViewModel>));
        }
    }
}
