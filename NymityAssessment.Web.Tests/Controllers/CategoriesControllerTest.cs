﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NymityAssessment.Domain.Entities;
using NymityAssessment.Domain.Interfaces;
using NymityAssessment.Web.Controllers;
using NymityAssessment.Web.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Web.Http;

namespace NymityAssessment.Web.Tests.Controllers
{
    [TestClass]
    public class CategoriesControllerTest
    {
        public readonly IUnitOfWork mockUnitOfWork;

        public CategoriesControllerTest()
        {
            AutoMapperConfig.Initialize();

            //Configure Mocks
            var category1 = new Category("Category 1", "");
            category1.SetCustomId(1);
            var category2 = new Category("Category 2", "");
            category2.SetCustomId(2);
            var categories = new List<Category>() { category1, category2 };

            Mock<IUnitOfWork> unitMoc = new Mock<IUnitOfWork>();

            //Setup some product repository methods
            unitMoc.Setup(m => m.Categories.GetById(It.IsAny<int>())).Returns((int i) => categories.FirstOrDefault(c => c.CategoryID == i));
            unitMoc.Setup(m => m.Categories.Find(It.IsAny<Expression<System.Func<Category, bool>>>())).Returns((Expression<System.Func<Category, bool>> criteria) => categories.Where(criteria.Compile()));

            this.mockUnitOfWork = unitMoc.Object;
        }

        [TestMethod]
        public void GetCategoryById()
        {
            var categoryId = 1;
            var category = this.mockUnitOfWork.Categories.GetById(categoryId);

            Assert.IsNotNull(category);
            Assert.IsInstanceOfType(category, typeof(Category));
            Assert.AreEqual(categoryId, category.CategoryID);
        }
        
        [TestMethod]
        public void GetCategoryByName()
        {
            var categories = this.mockUnitOfWork.Categories.Find(c => c.CategoryName== "Category 1");

            Assert.IsNotNull(categories);
            Assert.AreEqual(1, categories.Count());
        }

        [TestMethod]
        public void HttpGetCategory()
        {
            // Arrange
            var controller = new CategoriesController(this.mockUnitOfWork);

            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            // Act
            var response = controller.Get() as System.Web.Http.Results.OkNegotiatedContentResult<List<CategoryViewModel>>;

            // Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Content);
            Assert.IsInstanceOfType(response.Content, typeof(List<CategoryViewModel>));
        }

        [TestMethod]
        public void HttpGetCategoryById()
        {
            // Arrange
            var controller = new CategoriesController(this.mockUnitOfWork);

            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            // Act
            var response = controller.Get(1) as System.Web.Http.Results.OkNegotiatedContentResult<CategoryViewModel>;

            // Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Content);
            Assert.IsInstanceOfType(response.Content, typeof(CategoryViewModel));
        }
    }
}
