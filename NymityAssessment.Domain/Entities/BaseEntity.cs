﻿using NymityAssessment.Domain.Interfaces;

namespace NymityAssessment.Domain.Entities
{
    //As I usually use Guid for keys, I set the new Key on BaseEntity
    public class BaseEntity : IEntity
    {

    }
}