﻿using NymityAssessment.Domain.Interfaces;
using System;
using System.Collections.Generic;

namespace NymityAssessment.Domain.Entities
{
    public class Supplier : BaseEntity
    {
        protected Supplier()
        {

        }

        public Supplier(string companyName, string contactName, string contactTitle, string address, string city,
                        string region, string postalCode, string country, string phone, string fax, string homePage)
        {
            //The only not null field
            if (string.IsNullOrWhiteSpace(companyName))
                throw new ArgumentException(nameof(companyName));

            ContactName = contactName;
            ContactTitle = contactTitle;
            Address = address;
            City = city;
            Region = region;
            PostalCode = postalCode;
            Country = country;
            Phone = phone;
            Fax = fax;
            HomePage = homePage;
        }

        public int SupplierID { get; protected set; }
        public string CompanyName { get; protected set; }
        public string ContactName { get; protected set; }
        public string ContactTitle { get; protected set; }
        public string Address { get; protected set; }
        public string City { get; protected set; }
        public string Region { get; protected set; }
        public string PostalCode { get; protected set; }
        public string Country { get; protected set; }
        public string Phone { get; protected set; }
        public string Fax { get; protected set; }
        public string HomePage { get; protected set; }
    }
}