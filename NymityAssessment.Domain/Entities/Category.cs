﻿using System;

namespace NymityAssessment.Domain.Entities
{
    public class Category : BaseEntity
    {
        protected Category()
        {

        }

        public Category(string categoryName, string description)
        {
            //The only not null field
            if (string.IsNullOrWhiteSpace(categoryName))
                throw new ArgumentException(nameof(categoryName));

            CategoryName = categoryName;
            Description = description;
        }

        public int CategoryID { get; protected set; }
        public string CategoryName { get; protected set; }
        public string Description { get; protected set; }
        public byte[] Picture { get; protected set; }

        public void SetPicute(byte[] picture)
        {
            Picture = picture;
        }

        public void SetCustomId(int id)
        {
            CategoryID = id;
        }
    }
}