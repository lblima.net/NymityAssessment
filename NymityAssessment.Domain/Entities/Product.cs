﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NymityAssessment.Domain.Entities
{
    public class Product : BaseEntity
    {
        protected Product()
        {

        }

        //I would create a factory, but for simplicity I used this constructor.
        public Product(string productName, Supplier supplier, Category category, string quantityPerUnit = "", decimal unitPrice = 0,
                        Int16? unitsInStock = 0, Int16? unitsOnOrder = 0, Int16? reorderLevel = 0, bool discontinued = false)
        {
            //The only not null field
            if (string.IsNullOrWhiteSpace(productName))
                throw new ArgumentException(nameof(productName));

            ProductName = productName;
            QuantityPerUnit = quantityPerUnit;
            UnitPrice = unitPrice;
            UnitsInStock = unitsInStock;
            UnitsOnOrder = unitsOnOrder;
            ReorderLevel = reorderLevel;
            Discontinued = discontinued;

            Supplier = supplier;
            if (supplier != null)
                SupplierID = supplier.SupplierID;

            Category = category;
            if (category != null)
                CategoryID = category.CategoryID;
        }

        public int ProductID { get; protected set; }
        public string ProductName { get; protected set; }

        [ForeignKey("Supplier")]
        public int SupplierID { get; set; }
        public virtual Supplier Supplier { get; protected set; }

        [ForeignKey("Category")]
        public int CategoryID { get; set; }
        public virtual Category Category { get; protected set; }

        public string QuantityPerUnit { get; protected set; }
        public decimal? UnitPrice { get; protected set; }
        public Int16? UnitsInStock { get; protected set; }
        public Int16? UnitsOnOrder { get; protected set; }
        public Int16? ReorderLevel { get; protected set; }
        public bool Discontinued { get; protected set; }

        public void SetCustomId(int id)
        {
            ProductID = id;
        }
    }
}