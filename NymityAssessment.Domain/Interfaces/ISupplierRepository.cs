﻿using NymityAssessment.Domain.Entities;

namespace NymityAssessment.Domain.Interfaces
{
    public interface ISupplierRepository : IRepository<Supplier>
    {

    }
}