﻿using NymityAssessment.Domain.Entities;

namespace NymityAssessment.Domain.Interfaces
{
    public interface IProductRepository : IRepository<Product>
    {

    }
}