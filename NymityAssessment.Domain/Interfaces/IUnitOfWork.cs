﻿using System;
using System.Data.Entity;

namespace NymityAssessment.Domain.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IProductRepository Products { get; set; }
        ISupplierRepository Suppliers { get; set; }
        ICategoryRepository Categories { get; set; }

        DbContext Context { get; }

        int Complete();
    }
}