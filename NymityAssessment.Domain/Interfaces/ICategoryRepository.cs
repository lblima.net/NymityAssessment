﻿using NymityAssessment.Domain.Entities;

namespace NymityAssessment.Domain.Interfaces
{
    public interface ICategoryRepository : IRepository<Category>
    {

    }
}