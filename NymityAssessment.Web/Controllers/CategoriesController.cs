﻿using AutoMapper;
using NymityAssessment.Domain.Interfaces;
using NymityAssessment.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace NymityAssessment.Web.Controllers
{
    [Authorize]
    public class CategoriesController : ApiController
    {
        IUnitOfWork unitOfWork;

        public CategoriesController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET api/categories
        public IHttpActionResult Get()
        {
            try
            {
                var categories = unitOfWork.Categories.GetAll();
                var categoriesViewModel = Mapper.Map<List<CategoryViewModel>>(categories);

                return Ok(categoriesViewModel);
            }
            catch (Exception ex)
            {
                //Log ex...

                //Return custom menssage
                return BadRequest("Could not return the Categories");
            }
        }

        // GET api/categories/5
        public IHttpActionResult Get(int id)
        {
            try
            {
                var category = unitOfWork.Categories.GetById(id);

                if (category != null)
                {
                    var categoryViewModel = Mapper.Map<CategoryViewModel>(category);

                    return Ok(categoryViewModel);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                //Log ex...

                //Return custom menssage
                return BadRequest("Could not return the Category");
            }
        }
    }
}
