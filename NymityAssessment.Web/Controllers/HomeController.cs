﻿using NymityAssessment.Domain.Interfaces;
using System.Web.Mvc;
using System.Linq;

namespace NymityAssessment.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Products()
        {
            return View();
        }
    }
}