﻿using AutoMapper;
using NymityAssessment.Domain.Interfaces;
using NymityAssessment.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace NymityAssessment.Web.Controllers
{
    [Authorize]
    public class ProductsController : ApiController
    {
        IUnitOfWork unitOfWork;

        public ProductsController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET api/products
        public IHttpActionResult Get()
        {
            var products = unitOfWork.Products.GetAll();
            var productsViewModel = Mapper.Map<List<ProductViewModel>>(products);

            return Ok(productsViewModel);
        }

        // GET api/products/5
        public IHttpActionResult Get(int id)
        {
            try
            {
                var product = unitOfWork.Products.GetById(id);

                if (product != null)
                {
                    var productViewModel = Mapper.Map<ProductViewModel>(product);

                    return Ok(productViewModel);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                //Log ex...

                //Return custom menssage
                return BadRequest("Could not return the Product");
            }
        }

        // GET api/products/category/2
        [Route("api/products/category/{id}")]
        public IHttpActionResult GetProductsByCategory(int id)
        {
            try
            {
                var category = unitOfWork.Categories.GetById(id);

                if (category != null)
                {
                    var products = unitOfWork.Products.Find(p => p.CategoryID == id);
                    var productsViewModel = Mapper.Map<List<ProductViewModel>>(products);

                    return Ok(productsViewModel);
                }
                else
                {
                    return NotFound(); //I could return bad request here saying, category not found also.
                }
            }
            catch (Exception ex)
            {
                //Log ex...

                //Return custom menssage
                return BadRequest("Could not return the Products");
            }
        }
    }
}
