﻿using NymityAssessment.Domain.Entities;
using NymityAssessment.Web.ViewModels;

namespace NymityAssessment.Web
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            AutoMapper.Mapper.Initialize((config) =>
            {
                config.CreateMap<Product, ProductViewModel>().ReverseMap();
                config.CreateMap<Supplier, SupplierViewModel>().ReverseMap();
                config.CreateMap<Category, CategoryViewModel>().ReverseMap();
            });
        }
    }
}