﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NymityAssessment.Web.Startup))]
namespace NymityAssessment.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
