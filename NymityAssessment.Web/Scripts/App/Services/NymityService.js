﻿(function () {

    angular.module("NYMITYModule").factory("NYMITYDataService", function ($http) {

        var getCategories = function () {
            return $http.get(NYMITY.Root + "api/categories", {})
                .then(function (response) {
                    return response.data;
                });
        };

        var getCategory = function (id) {
            return $http.get(NYMITY.Root + "api/categories" + id)
                .then(function (response) {
                    return response.data;
                });
        };

        var getProductsByCategory = function (categoryID) {
            return $http.get(NYMITY.Root + "api/products/category/" + categoryID)
                .then(function (response) {
                    return response.data;
                });
        };

        return {
            getCategories: getCategories,
            getCategory: getCategory,
            getProductsByCategory: getProductsByCategory
        };
    });
}());