﻿(function () {

    var productController = function ($scope, NYMITYDataService) {

        $scope.category = {};
        $scope.categories = [];
        $scope.products = [];

        $scope.getCategories = function () {
            NYMITYDataService.getCategories()
                .then(function (response) {
                    $scope.categories = response;
                }, function (error) {
                    //Show error message
                });
        };

        $scope.getProductsByCategory = function (categoryID) {
            NYMITYDataService.getProductsByCategory(categoryID)
                .then(function (response) {
                    $scope.products = response;
                }, function (error) {
                    //Show error message
                });
        }

        $scope.getCategories();
    };

    angular.module("NYMITYModule").controller("ProductController", productController);
}());