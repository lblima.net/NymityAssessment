﻿var NYMITY = NYMITY || {};

NYMITY.Root = $("#rootPath").val();

(function () {

    var app = angular.module("NYMITYModule", []);

    angular.module('NYMITYModule').config(['$compileProvider', function ($compileProvider) {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|fil‌​e|ftp|blob):|data:im‌​age\//);
    }]);

}());