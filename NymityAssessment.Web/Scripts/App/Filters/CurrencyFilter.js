﻿(function () {

    angular.module('NYMITYModule').filter('CurrencyFilter', function () {
        return function (input) {
            var valor = '';

            if (input) {
                valor = numeral(input).format('$ 0.0,00');
            }
            else {
                valor = '';
            }

            return valor;
        }
    });
}());