﻿(function () {

    angular.module('NYMITYModule').filter('BooleanFilter', function () {
        return function (input) {
            var valor = '';

            if (input) {
                valor = "Yes"
            }
            else {
                valor = "No";
            }

            return valor;
        }
    });
}());