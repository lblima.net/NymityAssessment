﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NymityAssessment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NymityAssessment.Domain.Entities.Tests
{
    [TestClass()]
    public class CategoryTests
    {
        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void CreateCategoryWithoutName()
        {
            var category = new Category(null, "test category");
        }

        [TestMethod()]
        public void CreateCategoryWithName()
        {
            var category = new Category("new category name", "test category");

            Assert.IsNotNull(category);
        }
    }
}