﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace NymityAssessment.Domain.Entities.Tests
{
    [TestClass()]
    public class SupplierTests
    {
        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void CreateSuplierWithoutName()
        {
            var supplier = new Supplier(null, "Jhon", "Supervisor", "Stree 1", "Toronto", "On", "50000", "CA", "3434343434", "", "www.xxxxx.com");
        }

        [TestMethod()]
        public void CreateSuplierWithName()
        {
            var supplier = new Supplier("Canada Food", "Jhon", "Supervisor", "Stree 1", "Toronto", "On", "50000", "CA", "3434343434", "", "www.xxxxx.com");

            Assert.IsNotNull(supplier);
        }
    }
}