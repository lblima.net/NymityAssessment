﻿using NymityAssessment.Domain.Interfaces;
using NymityAssessment.Infrastructure.Context;
using NymityAssessment.Infrastructure.Repositories;

namespace NymityAssessment.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private NorthwindContext context;

        public UnitOfWork(NorthwindContext context)
        {
            this.context = context;

            Products = new ProductRepository(this.context);
            Suppliers = new SupplierRepository(this.context);
            Categories = new CategoryRepository(this.context);
        }

        public IProductRepository Products { get; set; }
        public ISupplierRepository Suppliers { get; set; }
        public ICategoryRepository Categories { get; set; }

        public System.Data.Entity.DbContext Context
        {
            get { return context; }
            protected set
            {
            }
        }

        public int Complete()
        {
            return context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}