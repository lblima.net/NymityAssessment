﻿using NymityAssessment.Domain.Entities;
using System.Data.Entity;

namespace NymityAssessment.Infrastructure.Context
{
    public class NorthwindContext : DbContext
    {
        public NorthwindContext() : base("NorthwindContext")
        {

        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}