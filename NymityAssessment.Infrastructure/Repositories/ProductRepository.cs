﻿using NymityAssessment.Domain.Entities;
using NymityAssessment.Domain.Interfaces;
using NymityAssessment.Infrastructure.Context;

namespace NymityAssessment.Infrastructure.Repositories
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        new NorthwindContext context;

        public ProductRepository(NorthwindContext context) : base(context)
        {
            this.context = context;
        }
    }
}