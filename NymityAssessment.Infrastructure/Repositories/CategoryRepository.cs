﻿using NymityAssessment.Domain.Entities;
using NymityAssessment.Domain.Interfaces;
using NymityAssessment.Infrastructure.Context;

namespace NymityAssessment.Infrastructure.Repositories
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        new NorthwindContext context;

        public CategoryRepository(NorthwindContext context) : base(context)
        {
            this.context = context;
        }
    }
}