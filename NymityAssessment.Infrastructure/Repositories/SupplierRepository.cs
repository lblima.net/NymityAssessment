﻿using NymityAssessment.Domain.Entities;
using NymityAssessment.Domain.Interfaces;
using NymityAssessment.Infrastructure.Context;

namespace NymityAssessment.Infrastructure.Repositories
{
    public class SupplierRepository : BaseRepository<Supplier>, ISupplierRepository
    {
        new NorthwindContext context;

        public SupplierRepository(NorthwindContext context) : base(context)
        {
            this.context = context;
        }
    }
}